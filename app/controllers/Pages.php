<?php
// Well can't get any of this working so no point doing it for other pages...
    class Pages extends Controller {

        public function __construct() {
            $this->postModel = $this->model('Post');
        }

        public function index() {
            // We want to display the most recent 5 acticles
            
            $test = $this->postModel->getAllTestData();
            //$t2 = $this->postModel->getTestData(0, 2);
            $a1 = $this->postModel->getIndexArticle(0);
            //$a2 = $this->postModel->getIndexArticle(1);
            //$a3 = $this->postModel->getIndexArticle(2);
            //$a4 = $this->postModel->getIndexArticle(3);
            //$a5 = $this->postModel->getIndexArticle(4);

            // What fucking numpty thought up php -> what type is this? How to I access it?
            // https://www.w3schools.com/php/php_arrays_multi.asp NOPE
            // http://php.net/manual/en/language.types.array.php NOT A CLUE
            // Guessing it's not an array welp, too late now
            
            $data = [
                //'title' => 'HOME',
                //'body' => 'Here is some random text....',
                //'image' => 'http://placekitten.com/g/400/400',
                'text' => $test,
                //'t2' => $t2
                'a1' => $a1,
                //'a2' => $a2,
                //'a3' => $a3,
                //'a4' => $a4,
                //'a5' => $a5
            ];

            $this->view('pages/index', $data);
        }


        public function about() {

            $data = [
                'title' => 'About us',
                'body' => 'Stuff about us',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/about', $data);
        }

        public function contact() {

            $data = [
                'title' => 'Contact us',
                'body' => 'Stuff about contacting us',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/contact', $data);
        }

        public function feedback() {

            $data = [
                'title' => 'Feedback',
                'body' => 'Stuff about giving us feedback',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/feedback', $data);
        }

        public function games() {

            $data = [
                'title' => 'Our Games',
                'body' => 'Stuff about our games',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/games', $data);
        }

        public function leaderboard() {

            $data = [
                'title' => 'Leaderboard',
                'body' => 'Stuff about the leaderboard',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/leaderboard', $data);
        }

        public function play() {

            $data = [
                'title' => 'Super Awesome Clicky Game',
                'body' => 'Super Awesome Clicky Game stuff',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/play', $data);
        }

        public function profile() {

            $data = [
                'title' => 'Your profile',
                'body' => 'Super Awesome PROFILE STUFF',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/profile', $data);
        }

        public function reviews() {

            $data = [
                'title' => 'Reviews',
                'body' => 'Super Awesome Clicky Game reviews',
                'image' => 'http://placekitten.com/g/500/500'
            ];

            $this->view('pages/reviews', $data);
        }

        public function addperson() {

            $data = [];

            if(!empty($_POST['fname']) && !empty($_POST['lname'])) {
                if($this->postModel->addPerson($_POST['fname'], $_POST['lname'])) {
                    $data = [
                        "title" => "Thank you for adding in a user"
                    ];
                } else {
                    die('SOMETHING WENT WRONG!');
                }
            } else {
                $data = [
                    "title" => "Please add a user"
                ];
            }

            $this->view('pages/addperson', $data);

        }

    }
?>