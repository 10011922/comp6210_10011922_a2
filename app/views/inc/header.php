<!DOCTYPE html>
<html lang="en">
<head>
<!--
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">    
    <title><?php echo SITENAME; ?></title>
    -->

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo SITENAME; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/skeleton-framework/1.1.1/skeleton.css">-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
    <link rel="stylesheet" href="<?php echo URLROOT; ?>/css/style.css">
    <script src="main.js"></script>

</head>
<body>





<!--
<?php
echo "HERE IS THE HEADER";
?>
<div class="container" style="max-width:1920">
<!-- TOP BAR --
    <div class="row" style="height:180px">
        <div class="two columns" style="background-color:red; border-style:solid; border-width:1px; border-color:black; height:100%">
            <img src="../src/img/tst.jpeg" alt="testimage" style="width:100%">
<!--            <div class="row">
                <div class="two columns" style="background-color:lightblue">HOME</div>
                <div class="two columns" style="background-color:lightblue">GAMES</div>
                <div class="two columns" style="background-color:lightblue">REVIEWS</div>
                <div class="two columns" style="background-color:lightblue">PLAY</div>
                <div class="two columns" style="background-color:lightblue">PROFILE</div>
                <div class="two columns" style="background-color:lightblue">LEADERBOARD</div>
            </div> --
        </div>
        <div class="ten columns" style="background-color:blue" style="height:100%">
            <div class="row" style="height:60px">
                <div class="ten columns"></div>
                <div class="one column"><p>LOG IN</p></div>
                <div class="one column"><p>SIGN UP</p></div>
            </div>
            <div class="row" style="height:120px">
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/index'">HOME</button></div>
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/games'">GAMES</button></div>
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/reviews'">REVIEWS</button></div>
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/play'">PLAY</button></div>
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/profile'">PROFILE</button></div>
                <div class="two columns" style="background-color:lightblue"><button class="btn btn-primary" onclick="window.location.href='../pages/leaderboard'">LEADERBOARD</button></div>
                
            </div>
        </div>
        <!--<div class="one column">LOG IN</div>
        <div class="one column">SIGN UP</div> --
    </div>


</div>
-->

<div class="container-fluid"> <!-- not indenting this -->
<!-- TOP BAR -->
<div class="row">
    <div class="col-6 col-sm-4">
        <img src="../../public/images/tst.jpeg" class="img-fluid" alt="Responsive image">
    </div>
    <div class="col-6 col-sm-8">
        <!-- TOP ROW - LOGIN & SIGNUP -->
        <div class="row justify-content-end" style="height:50%">
                    
            <!--<div class="col-10 col-sm-2">-->
                <nav class="navbar navbar-expand navbar-light nav-fill w-100">
                    <a class="navbar-brand" href="#"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav  nav-fill w-100">
                            <a class="nav-item nav-link" href="#">LOG IN</a>
                            <a class="nav-item nav-link" href="#">SIGN UP</a>
                        </div>
                    </div>
                </nav>
            <!--</div>-->
        </div>
        <!-- BOTTOM ROW - NAV ITEMS -->
        <div class="row" style="height:50%">
            <nav class="navbar navbar-expand-sm navbar-light  nav-fill w-100">
                <a class="navbar-brand" href="#"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                    <div class="navbar-nav  nav-fill w-100">
                        <a class="nav-item nav-link active" href="../pages/index">Home <span class="sr-only">(current)</span></a>
                        <a class="nav-item nav-link" href="../pages/games">Games</a>
                        <a class="nav-item nav-link" href="../pages/reviews">Reviews</a>
                        <a class="nav-item nav-link" href="../pages/play">Play</a>
                        <a class="nav-item nav-link" href="../pages/profile">Profile</a>
                        <a class="nav-item nav-link" href="../pages/leaderboard">Leaderboard</a>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</div>





<div class="row">
    <div class="col-1 col-md-2"></div>