/****       DELETE DATABASE     ****/
-- DROP DATABASE containerdb

/****       CREATE & USE DATABASE     ****/
USE mysql;
-- CREATE DATABASE containerdb2;
/*
USE containerdb;
GO

/****       CREATE TABLES     ****
-- USERS
CREATE TABLE tbl_user (
    ID int NOT NULL,
    USRNAME varchar(16) NOT NULL,
    PASS varchar(32) NOT NULL,
    AVATARURL nvarchar(255) DEFAULT 'http://placekitten.com/g/128/128',
    CAPTION nvarchar(255) DEFAULT 'What is best in life? To crush your enemies, see them driven before you, and to hear the lamentation of their women.',
    PRIMARY KEY (ID)
) AUTO_INCREMENT 1
USE containerdb;

INSERT INTO tbl_user (USRNAME, PASS) VALUES ("BOBBY", "LOL")
INSERT INTO tbl_user (USRNAME, PASS) VALUES ("TIMMY", "LOL")
INSERT INTO tbl_user (USRNAME, PASS) VALUES ('JIMMY', 'LOL')
INSERT INTO tbl_user (USRNAME, PASS) VALUES ('LOL', 'LOL')
SELECT * FROM tbl_user

-- WEBSITE TEXT
-- Wanted PAGEID int DEFAULT 0
CREATE TABLE tbl_webtext (
    ID int AUTO_INCREMENT,
    PID int DEFAULT 0,
    CONTENT varchar(8000) DEFAULT 'DEFAULT PAGE CONTENT'
) AUTO_INCREMENT = 1

-- WEBSITE IMAGES
CREATE TABLE tbl_webimg (
    ID int AUTO_INCREMENT,
    PATH varchar(511) DEFAULT 'http://placekitten.com/g/128/128'
)

-- LEADERBOARD
CREATE TABLE tbl_scores ( -- Depends on user
    ID int AUTO_INCREMENT,
    USRID int NOT NULL,
    NUMCLICKS int NOT NULL
) AUTO_INCREMENT = 1

-- REVIEWS
CREATE TABLE tbl_reviews ( -- Depends on user
    ID int AUTO_INCREMENT,
    USRID int NOT NULL,
    RATING int CHECK (SCORE >= 0 AND SCORE <= 11) NOT NULL,
    BODY varchar(500)
) AUTO_INCREMENT = 1





INSERT INTO tbl_users (USRNAME, PASS, AVATARURL, CAPTION) VALUES (
    'Bjarne Stroustrup', 'cpp', 'http://stereobooster.github.io/assets/posts/two-big-schools-of-object-oriented-programming/bjarne-stroustrup.png', 'C makes it easy to shoot yourself in the foot; C++ makes if harder, but when you do it blows your whole leg off',
    'Steve Wozniak', 'apple', 'https://www.gannett-cdn.com/-mm-/5807ae17d21f3c6e05cc378de34797f9534f9737/c=294-0-4905-3467&r=x404&c=534x401/local/-/media/2018/04/08/USATODAY/USATODAY/636587994185506016-XXX-134451-008.jpg', 'Never trust a computer you cant throw out a window');
--INSERT INTO tbl_t (FNAME, LNAME) VALUES ('bob', 'stevenson');
--INSERT INTO tbl_test2 (FNAME, LNAME) VALUES ('ella', 'stevenson');

SELECT * FROM tbl_users

*/