-- USE mysql;
-- DROP DATABASE containerdb2

-- CREATE DATABASE containerdb2;

USE containerdb2;


-- DROP TABLE tbl_user;
-- USERS
CREATE TABLE tbl_user (
    ID int NOT NULL AUTO_INCREMENT,
    USRNAME nvarchar(16) NOT NULL,
    PASS nvarchar(32) NOT NULL,
    AVATARURL nvarchar(255) DEFAULT 'http://placekitten.com/g/128/128',
    CAPTION nvarchar(255) DEFAULT 'What is best in life? To crush your enemies, see them driven before you, and to hear the lamentation of their women.',
    PRIMARY KEY (ID)
);
-- 0 - No page/testing
-- 1 - Index
-- 2 - Games
-- 3 - Reviews
-- 4 - Play
-- 5 - Profile
-- 6 - Leaderboards
-- 7 - Login
-- 8 - Signup
-- 9 - Aboutus
--10 - Feedback
--11 - Contact
USE containerdb2;
CREATE TABLE tbl_webtext (
    ID int NOT NULL AUTO_INCREMENT,
    PID int NOT NULL DEFAULT 0,
    CONTENT varchar(8000) NOT NULL DEFAULT 'DEFAULT PAGE CONTENT',
    PRIMARY KEY(ID)
);

USE containerdb2;
-- DROP TABLE tbl_webtext;
SELECT * FROM tbl_webtext;

-- Insert data

INSERT INTO tbl_user (USRNAME, PASS) VALUES ('BOBBY', 'LOL');
INSERT INTO tbl_user (USRNAME, PASS) VALUES ('TIMMY', 'LOL');
INSERT INTO tbl_user (USRNAME, PASS) VALUES ('JIMMY', 'LOL');
INSERT INTO tbl_user (USRNAME, PASS) VALUES ('LOL', 'LOL');


USE containerdb2;
-- TEST
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (0, 'THIS IS TEST DATALOLOLOLOLOLOL');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (0, 'Consectetur adipiscing elit. Maecenas dapibus elit metus, ac finibus sapien blandit nec. Sed cursus id neque et viverra. Curabitur vulputate, ante et lobortis aliquet, nibh nisl convallis justo, at dictum tortor augue at magna. Maecenas vestibulum ligula sem. Phasellus sodales magna non tortor malesuada, sed rhoncus tortor varius. Nulla facilisi. Nulla vel ex commodo, consequat est nec, efficitur elit. Donec faucibus nec mi sit amet maximus. Vivamus et luctus turpis. Suspendisse potenti. Vivamus quis aliquam sapien. Donec scelerisque turpis a scelerisque scelerisque. Sed commodo lorem nulla, eget semper odio luctus in. Live Reload is not possible without body or head tag.');
INSERT INTO tbl_webtext (PID) VALUES (0);
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (0, 'HELLLLOOOOOOOOOOOOOOOOOOOOOOO');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (0, 'If I ever have to use these tools again Ill gut someone');

-- INDEX
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (1, 'asdfsadf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (1, 'sdfasdf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (1, 'asdfasdf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (1, 'dasfeawfe');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (1, 'awefwef');

-- GAMES
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (2, 'weqfwef');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (2, 'qwefqwef');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (2, 'wefwqef');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (2, 'qwefqwef');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (2, 'sdfasdf');

-- REVIEWS
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (3, 'wdqwdqwdq');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (3, 'werfqadf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (3, 'adsfasdf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (3, 'asdfasdf');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (3, 'asdfasdf');

-- PLAY
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (4, 'djyryukryuk');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (4, 'ryukrhyjnrfyuh');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (4, 'nsfrtjnfgytj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (4, 'tfyjkedtyhjedtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (4, 'edtyjnetyjedtyj');

-- PROFILE
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (5, 'ytejtyhjntdfygjdty');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (5, 'ytedjyhryjuetdyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (5, 'etyjnetydfyhjnder5');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (5, 'yedjntfjnytfyjuedtygjn');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (5, 'etyjnythjntyjtyeje');

-- LEADERBOARDS
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (6, 'etyhytfrgje56ujhetyjh');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (6, 'rfjyedts6uhdyjn');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (6, 'teyjnhttgfntjtdy');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (6, 'etdyjndtfygjn');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (6, 'srtjhngfnjtyjt');

-- LOGIN
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (7, 'ygtje5tyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (7, 'ytjdeyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (7, 'etyjnetyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (7, 'etyjetyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (7, 'eyjetyje');

-- SIGNUP
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (8, 'yjtyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (8, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (8, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (8, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (8, 'tyjtyj');

-- ABOUT
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (9, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (9, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (9, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (9, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (9, 'tyjtyj');

-- FEEDBACK
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (10, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (10, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (10, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (10, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (10, '');

-- CONTACT
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (11, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (11, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (11, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (11, 'tyjtyj');
INSERT INTO tbl_webtext (PID, CONTENT) VALUES (11, 'tyjtyj');
